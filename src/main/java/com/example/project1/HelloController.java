package com.example.project1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/v1/hello")
	public String greeting() {
		return "Hello World";
	}

	// path variable
	@GetMapping("/v1/hello/{name}")
	public String greetingWithName(@PathVariable String name) {
		return "Hello " + name;
	}

	@GetMapping("/v2/hello/{name}/{lastname}")
	public String greetingWithNameLastnamev2(
			@PathVariable String name, 
			@PathVariable String lastname) {
		return greetingWithNameLastname(name, lastname);
	}

	@GetMapping("/v2/hello/{name}")
	public String greetingWithNameLastname(@PathVariable String name, String lastname) {
		return "Hello " + name + " " + lastname;
	}

	@GetMapping("/v1/hello-name")
	public String greeting(String name) {
		return "Hello " + name;
	}

}
