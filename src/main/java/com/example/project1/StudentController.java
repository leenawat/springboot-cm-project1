package com.example.project1;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
	
	@Autowired
	StudentRepository studentRepository;
	
	@GetMapping("/v1/student")
	public List<Map<String, Object>> findAll() {
		return studentRepository.findAll();
	}
	
	@GetMapping("/v2/student")
	public List<Student> findAll2() {
		return studentRepository.findAll2();
	}
	
	@GetMapping("/v1/student/{id}")
	public Map<String, Object> findById(
			@PathVariable int id) {
		return studentRepository.findById(id);
	}
	
	@GetMapping("/v2/student/{id}")
	public Student findById2(
			@PathVariable int id) {
		return studentRepository.findById2(id);
	}
	
	@GetMapping("/v1/student/{name}/{age}")
	public Map<String, Object> findById(
			@PathVariable String name, 
			@PathVariable int age) {
		return studentRepository.find(name, age);
	}
	
	@PostMapping("/v1/student")
	public int save(@RequestBody Student student) {
		return studentRepository.save(student);
	}
	
	@PutMapping("/v1/student/{id}")
	public int update(
			@PathVariable int id, 
			@RequestBody Student student) {
		return studentRepository.update(id, student);
	}
	
	@DeleteMapping("/v1/student/{id}")
	public int delete(@PathVariable int id) {
		return studentRepository.delete(id);
	}

}
