package com.example.project1;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Map<String, Object>> findAll() {
		String sql = "select * from student";
		return jdbcTemplate.queryForList(sql);
	}
	
	public List<Student> findAll2() {
		String sql = "select * from student";
		return jdbcTemplate.query(
				sql,
				new BeanPropertyRowMapper<>(Student.class)
		);
	}
	
	public Map<String, Object> findById(int id) {
		String sql = "select * from student where id = ?";
		return jdbcTemplate.queryForMap(sql, new Object[] { id });
	}

	public Student findById2(int id) {
		String sql = "select * from student where id = ?";
				return jdbcTemplate.queryForObject(
				sql, 
				new Object[] { id }, 
				new BeanPropertyRowMapper<>(Student.class));
	}
	
	public Map<String, Object> find(String name, int age) {
		String sql = "select * from student ";
		sql += "where name = ? and age = ?";
		return jdbcTemplate.queryForMap(
				sql, 
				new Object[] { name, age }
				);
	}
	
	public int save(Student student) {
		String sql = "INSERT INTO `student`(`name`,`age`) VALUES(?, ?)";
		return jdbcTemplate.update(
			sql, 
			new Object[] {student.getName(), student.getAge()}
			);
	}
	
	public int update(int id, Student student) {
		String sql = "update student ";
		sql += "set name = ?, age = ? ";
		sql += "where id = ?";
		return jdbcTemplate.update(
			sql, 
			new Object[] {
				student.getName(), 
				student.getAge(), 
				id
			}
		);
	}
	
	public int delete(int id) {
		return jdbcTemplate.update(
			"delete from student where id = ?",
			new Object[] {id}
		);
	}

}
